SITE_DIR=site
all:
	#python render.py index.html index.json > $(SITE_DIR)/index.html
	python render.py regions.html > $(SITE_DIR)/regions.html
	python render.py index.html > $(SITE_DIR)/index.html
	python render.py alltours.html > $(SITE_DIR)/alltours.html
	python render.py contacts.html > $(SITE_DIR)/contacts.html
	python render.py tour_page.html > $(SITE_DIR)/tour_page.html
	cp -r static/* $(SITE_DIR)/
