(function($) {
var icelandMap = AmCharts.makeChart("map", {
  "type": "map",
  "pathToImages": "http://www.amcharts.com/lib/3/images/",
  "addClassNames": true,
  "fontSize": 15,
  "color": "#FFFFFF",
  "projection": "mercator",
  "backgroundAlpha": 1,
  "backgroundColor": "rgba(80,80,80,0)",
  "dragMap": false,
  "zoomOnDoubleClick": false,
  "dataProvider": {
    "map": "icelandLow",
    "getAreasFromMap": true,
    "images": [
        {
            "top": 40,
            "left": 60,
            "width": 80,
            "height": 40,
            "pixelMapperLogo": true,
            "imageURL": "",
            "url": ""
        },
        
        {
            "selectable": true,
            "title": "Reykjavík",
            "longitude": -21.5872,
            "latitude": 64.1298,
            "svgPath": "M8.944,1C11.976,1,14.718,2.588,15.99,5.064C17.264,2.589,20.039,1,23.072,1C27.811,1,34.166,4.82,31.272,15.242C27.966,25.43,15.99,31.419,15.99,31.417C15.99,31.419,4.033,25.43,0.726,15.242C-2.168,4.82,4.205,1,8.944,1C8.944,1,8.944,1,8.944,1",
            "color": "rgba(232,131,245,1)",
            "scale": 0.7
        },
        
    ],
    "areas": [
        {
            "id": "GL1",
            "title": "Glacier",
            "color": "rgba(200,234,255,1)"
        },
        {
            "id": "GL2",
            "title": "Glacier",
            "color": "rgba(200,234,255,1)"
        },
        {
            "id": " GL9",
            "title": "Vatnajokulsthjodgardur (Glacier)",
            "color": "rgba(200,234,255,1)"
        },
        {
            "id": "IS-1",
            "title": "Reykjavík",
            "color": "rgba(87,115,156,1)"
        },
        {
            "id": "IS-2",
            "title": "Reykjanes",
            "color": "rgba(87,115,156,1)"
        },
        {
            "id": "IS-3",
            "title": "Vesturland or West Iceland",
            "color": "rgba(87,115,156,1)"
        },
        {
            "id": "IS-4",
            "title": "Vestfirðir or Westfjords",
            "color": "rgba(87,115,156,1)"
        },
        {
            "id": "IS-5",
            "title": "Norðurland vestra or North West",
            "color": "rgba(87,115,156,1)"
        },
        {
            "id": "IS-6",
            "title": "Norðurland eystra or Norh East",
            "color": "rgba(87,115,156,1)"
        },
        {
            "id": "IS-7",
            "title": "Austurland or East Iceland",
            "color": "rgba(87,115,156,1)"
        },
        {
            "id": "IS-8",
            "title": "Suðurland or South Iceland",
            "color": "rgba(87,115,156,1)"
        }
    ]
  },
  "balloon": {
      "horizontalPadding": 15,
      "borderAlpha": 0,
      "borderThickness": 1,
      "verticalPadding": 15
  },
  "areasSettings": {
      "color": "rgba(89,126,179,1)",
      "outlineColor": "rgba(80,80,80,0)",
      "rollOverOutlineColor": "rgba(80,80,80,0)",
      "rollOverBrightness": 20,
      "selectedBrightness": 20,
      "selectable": true,
      "unlistedAreasAlpha": 0,
      "unlistedAreasOutlineAlpha": 0
  },
  "imagesSettings": {
      "alpha": 1,
      "color": "rgba(89,126,179,1)",
      "outlineAlpha": 0,
      "rollOverOutlineAlpha": 0,
      "outlineColor": "rgba(80,80,80,0)",
      "rollOverBrightness": 20,
      "selectedBrightness": 20,
      "selectable": true
  },
  "linesSettings": {
      "color": "rgba(89,126,179,1)",
      "selectable": true,
      "rollOverBrightness": 20,
      "selectedBrightness": 20
  },
  "zoomControl": {
      "zoomControlEnabled": false,
      "homeButtonEnabled": false,
      "panControlEnabled": false,
      "right": 38,
      "bottom": 30,
      "minZoomLevel": 0.25,
      "gridHeight": 100,
      "gridAlpha": 0.1,
      "gridBackgroundAlpha": 0,
      "gridColor": "#FFFFFF",
      "draggerAlpha": 1,
      "buttonCornerRadius": 2
  }
});

icelandMap.addListener('clickMapObject', function(event) {
  var regionId = event.mapObject.id;
  $('.region-details').hide();
  var viewId = '#region_details_' + regionId;
  location.hash = viewId;
  var detailsView = $(viewId);
  detailsView.show();
  detailsView.get()[0].scrollIntoView({'behavior': 'smooth'});
});

$('.carousel').carousel({interval: false});

})(jQuery);
