(function($){
    "use strict";

//iceland facts navigation
$(document).on('click', '.article-nav a', function(event) {
	event.preventDefault();
	var link = $(this).attr("href");
	var holders = $('.articleholder');

	holders.fadeOut(400);

	setTimeout(function(){
		holders.siblings('[data-article="'+link+'"]').fadeIn(400);
	},400);

	$(this).parent().siblings('li').removeClass('active');
	$(this).parent().addClass('active'); 
});

//fav button clicks
$(document).on('click', '.fav-button', function(event) {
  event.preventDefault();
  $(event.target).toggleClass('fa-heart fa-heart-o is-fav');
  $(event.target).blur();
});

$('.facts-menu-items a')
.add('.facts-menu-xs a').click(function(event) {
  event.preventDefault();
  var $target = $(event.target);
  $('.fact').hide().toggleClass('active');
  $('.facts-menu-items a').removeClass('active');
  $target.toggleClass('active');
  $($target.attr('href')).show();
});

// change is-checked class on buttons
$('.button-group').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'button', function() {
    $buttonGroup.find('.is-checked').removeClass('is-checked');
    $( this ).addClass('is-checked');
  });
});

var $navBar = $('.menu-wrapper');
/*$(window).scroll(function(e) {
  if ($(window).scrollTop() > $('#header_block')) {
    $navBar.toggleClass('menu-wrapper-white');
  }
});*/

var mainbottom = $('#header_block').height();

// on scroll, 
$(window).on('scroll',function(){

    // we round here to reduce a little workload
    var stop = Math.round($(window).scrollTop());

    if (stop > mainbottom) {
        $navBar.addClass('menu-wrapper-white');
    } else {
        $navBar.removeClass('menu-wrapper-white');
    }

});

$('#hamburger').click(function() {
  if ($('#top-menu-items').is(':visible')) {
    $navBar.removeClass('menu-wrapper-white');
  } else {
    $navBar.addClass('menu-wrapper-white');
  }
});

})(jQuery);
