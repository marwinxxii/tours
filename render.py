import sys
import json

from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader('templates'))

if len(sys.argv) > 2:
    with open(sys.argv[2], 'r') as f:
        args = json.load(f)
else:
    args = {}

print(env.get_template(sys.argv[1]).render(args))
